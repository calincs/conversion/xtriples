FROM frekele/ant:1.10.3-jdk8 AS build
WORKDIR /root
COPY . .
RUN ant xar

FROM existdb/existdb:5.4.0
COPY --from=build /root/build/xtriples-1.4.xar /exist/autodeploy
COPY shared-resources-0.9.1.xar /exist/autodeploy
