// Show file name on file select and update it
function updateFileName() {
    if (document.getElementById('fileInput').files[0] !== undefined) {
        document.getElementById("downloadButton").classList.remove("form-btn-disabled");
        document.getElementById("downloadButton").classList.add("form-btn");
        document.getElementById('forFileName').style.display = "block";
        document.getElementById('fileName').innerHTML = document.getElementById("fileInput").files[0].name;
    }
}

// Download file in xml/ttl
function downloadURI() {
    console.log('document.getElementById("transform").value => ', document.getElementById("transform").value);

    let fileNewName = "";
    let showDownloaded = document.getElementsByClassName("file-downloaded");
    let headers = {};
    var valueStored = document.querySelector('input[name="radio"]:checked').value;
    if (valueStored === "turtle") {
        fileNewName = `${(document.getElementById('fileInput').files[0].name).split('.').slice(0, -1).join('.')}.ttl`;
        headers = {
            'Content-Type': 'application/xml',
            'format': 'turtle',
            'Transform': document.getElementById("transform").value
        }
    } else if (valueStored === "none") {
        fileNewName = `${(document.getElementById('fileInput').files[0].name).split('.').slice(0, -1).join('.')}.rdf`;
        headers = {
            'Content-Type': 'application/xml',
            'format': 'none',
            'Transform': document.getElementById("transform").value
        }
    }
    var selectedFile = document.getElementById('fileInput').files[0];
    var reader = new FileReader();
    var readXml = null;
    let documentToUpload = null;
    reader.onload = function (e) {
        readXml = e.target.result;
        var parser = new DOMParser();
        documentToUpload = parser.parseFromString(selectedFile, "application/xml");
    }
    reader.readAsText(selectedFile);
    const url = window.location.href.replace('index.html', 'extract.xql');
    setTimeout(() => {
        fetch(url, {
            method: 'POST',
            body: readXml,
            headers: headers,
        }).then(response => {
            if (response.status === 200) {
                response.text().then(data => {
                    showDownloaded[0].style.visibility = "visible";
                    showDownloaded[0].style.display = "inline-flex";
                    document.getElementById("fileDownloaded").innerHTML = `${fileNewName} downloaded`;
                    var pom = document.createElement('a');
                    var bb = new Blob([data], { type: 'text/plain' });
                    pom.setAttribute('href', window.URL.createObjectURL(bb));
                    pom.setAttribute('download', fileNewName);
                    pom.dataset.downloadurl = ['text/plain', pom.download, pom.href].join(':');
                    pom.click();
                });
            } else {
                document.getElementById("forFileName").style.display = "none";
                showDownloaded[0].style.visibility = "visible";
                document.getElementById("fileDownloaded").innerHTML = "There was an error while downloading your file. Please try again.";
                document.getElementById("fileDownloaded").innerHTML.style.color = "red";
            }
        });
    }, 2000);
}