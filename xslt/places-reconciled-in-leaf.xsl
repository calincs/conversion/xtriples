<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="http://iflastandards.info/ns/fr/frbr/frbroo/" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            <xsl:apply-templates select="descendant::listPlace"/>
        </rdf:RDF>
    </xsl:template>
    
    <!-- TODO Add cwrc URIs + process <note> with the word doc inside LINCS Drive -->
    
    <xsl:template match="listPlace">
        <xsl:apply-templates select="place"/>
    </xsl:template>
    
    <xsl:template match="place">
        <xsl:element name="{concat('crm:', 'E53_Place')}">
            <xsl:attribute name="rdf:about" select="child::placeName[@type='standard']/@ref"></xsl:attribute>
            
            <rdfs:label>
                <xsl:value-of select="normalize-space(placeName[@type='standard'])"/>
            </rdfs:label>
            
            <crm:P2_has_type>
                <xsl:element name="{concat('crm:', 'E55_Type')}">
                    <xsl:attribute name="rdf:about" select="@type"/>
                </xsl:element>
            </crm:P2_has_type>
            
            <crm:P1_is_identified_by>
            <xsl:choose>
                <xsl:when test="placeName[@type='preferred']/text()">
                    <xsl:apply-templates select="placeName[@type='preferred']"/>
                </xsl:when>                                     
            </xsl:choose>
            
            <xsl:choose>
                <xsl:when test="placeName[@type='variant']/text()">
                    <xsl:apply-templates select="placeName[@type='variant']"/>
                </xsl:when>                                     
            </xsl:choose>             
            
            <xsl:apply-templates select="location"/>
            
            <!--About <trait> and <event type="establishment/dissolution">: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            </crm:P1_is_identified_by>
            
            <xsl:apply-templates select="descendant::geo"/>
        </xsl:element>
    </xsl:template>
    
    
    <xsl:template match="placeName[@type='preferred']">        
        <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Name_of_Place">
            <!-- needs to be different that the Place URIs. Jessica says "E53 main Place URIS" and "Name of place URIs" are 2 diff things, which makes sense. -->
                <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/></rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://temp.lincsproject.ca/Place_Name_Type"/>
                </crm:P2_has_type>
                
                <xsl:apply-templates select="child::name[@type='full']"/>
                
            </crm:E33_E41_Linguistic_Appellation>
    </xsl:template>
    
    <xsl:template match="name[@type='full']">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Fullname_of_place?">
                <rdfs:label>Fullname of <xsl:value-of select="normalize-space(ancestor::placeName[@type='preferred'])"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://temp.lincsproject.ca/Full_Place_Name_Type?"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>
    </xsl:template>
    
    <xsl:template match="placeName[@type='variant']">
        <xsl:apply-templates select="child::name[@type='deprecated']"/>
    </xsl:template>
    
    <xsl:template match="name[@type='deprecated']">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Deprecated_name_of_place?">
                <rdfs:label>Deprecated name of <xsl:choose>
                    <xsl:when test="preceding-sibling::placeName[@type='preferred']/text()">
                        <xsl:value-of select="normalize-space(preceding-sibling::placeName[@type='preferred'])"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(preceding-sibling::placeName[@type='standard'])"/>
                    </xsl:otherwise>                       
                </xsl:choose>
                </rdfs:label>
                
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://temp.lincsproject.ca/Deprecated_Place_Name_Type?"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>
    </xsl:template>
    
    <xsl:template match="location">
        <crm:E41_Appellation rdf:about="http://temp.lincsproject.ca/Address_of_place">
            <rdfs:label> Address of <xsl:choose>
                <xsl:when test="preceding-sibling::placeName[@type='preferred']/text()">
                    <xsl:value-of select="normalize-space(preceding-sibling::placeName[@type='preferred'])"/>
                </xsl:when>
                <xsl:otherwise>
                    <xsl:value-of select="normalize-space(preceding-sibling::placeName[@type='standard'])"/>
                </xsl:otherwise>                       
            </xsl:choose>
            </rdfs:label>
            
            <xsl:apply-templates select="address"/>           
            
        </crm:E41_Appellation>
    </xsl:template>
    
    <xsl:template match="address">
        <crm:P190_has_symbolic_content xml:lang="en">
            <xsl:value-of select="normalize-space(.)"/>
        </crm:P190_has_symbolic_content>
        <crm:P2_has_type>
            <crm:E55_Type rdf:about="http://id.lincsproject.ca/eml/Address"/>
        </crm:P2_has_type>
    </xsl:template>
    
    <xsl:template match="geo">
        <crm:P168_place_is_defined_by>
            <xsl:value-of select="normalize-space(.)"/>
        </crm:P168_place_is_defined_by>
    </xsl:template>
</xsl:stylesheet>
