<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="http://iflastandards.info/ns/fr/frbr/frbroo/" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:cwrc="http://sparql.cwrc.ca/ontologies/cwrc.html"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    xmlns:dc="https://www.dublincore.org/specifications/dublin-core/dcmi-namespace/"
    exclude-result-prefixes="cwrc xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
    xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> 
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            
            <xsl:apply-templates select="descendant::body"/>
            
        </rdf:RDF>
    </xsl:template>
    
    <xsl:template match="body">        
        <xsl:apply-templates select="./div[@type='transcription']"/>
    </xsl:template>
    
    <xsl:template match="div[@type='transcription']">   
        <xsl:element name="{concat('crm:', 'E73_Information_Object')}">
            <xsl:attribute name="rdf:about" select="'http://temp.lincsproject.ca/SampleLetterURI'"/> <!-- idno/@n insert <idno> when available -->
            <rdfs:label>Letter from <xsl:value-of select="normalize-space(preceding::head/persName[1])"/> to <xsl:value-of select="normalize-space(preceding::head/persName[2])"/></rdfs:label>                    
           
            <xsl:apply-templates select="descendant::persName"/>
            <xsl:apply-templates select="descendant::placeName"/>
            
        </xsl:element>
            
    </xsl:template>
    
    <xsl:template match="descendant::persName">
        <crm:P67_refers_to>
            <xsl:element name="{concat('crm:', 'E21_Person')}">
                <xsl:attribute name="rdf:about" select="@ref"/>
                <rdfs:label><xsl:value-of select="normalize-space(.)"/></rdfs:label>
                </xsl:element>           
        </crm:P67_refers_to>
    </xsl:template>
    
    <xsl:template match="descendant::placeName">
        <crm:P67_refers_to>
            <xsl:element name="{concat('crm:', 'E53_Place')}">
                <xsl:attribute name="rdf:about" select="@ref"/>
                <rdfs:label><xsl:value-of select="normalize-space(.)"/></rdfs:label>
            </xsl:element>           
        </crm:P67_refers_to>
    </xsl:template>
</xsl:stylesheet>
