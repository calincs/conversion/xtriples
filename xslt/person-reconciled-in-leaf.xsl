<?xml version="1.0" encoding="UTF-8"?>
<xsl:stylesheet xmlns:xsl="http://www.w3.org/1999/XSL/Transform" xmlns:schema="https://schema.org/"
    xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
    xmlns:xs="http://www.w3.org/2001/XMLSchema" xmlns:tei="http://www.tei-c/ns/1.0"
    xmlns:frbroo="http://iflastandards.info/ns/fr/frbr/frbroo/" 
    xmlns:owl="http://www.w3.org/TR/2004/REC-owl-guide-20040210/"
    xmlns:fa="http://www.w3.org/2005/xpath-functions"
    xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
    xmlns:map="http://www.w3.org/2005/xpath-functions/map"
    exclude-result-prefixes="xsl schema rdf tei fa owl xs"
    xpath-default-namespace="http://www.tei-c.org/ns/1.0" version="2.0"
       xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
    <xsl:output method="xml" encoding="UTF-16" indent="yes"/> <!--declare frbroo-->
    
    <xsl:template match="/">
        <rdf:RDF xml:lang="en"   
            xmlns:rdf="http://www.w3.org/1999/02/22-rdf-syntax-ns#"
            xmlns:crm="http://www.cidoc-crm.org/cidoc-crm/"
            xmlns:skos="http://www.w3.org/2004/02/skos/core#"
            xmlns:temp_lincs_temp="http://temp.lincsproject.ca/"
            xmlns:rdfs="http://www.w3.org/2000/01/rdf-schema#">
            <xsl:apply-templates select="descendant::listPerson"/>
        </rdf:RDF>
    </xsl:template>   
 
    
    <xsl:template match="listPerson">
        <xsl:apply-templates select="person"/>
    </xsl:template>
    
    <xsl:template match="person">
        <xsl:element name="{concat('crm:', 'E39_Actor')}">
            <xsl:attribute name="rdf:about" select="child::persName[@type='standard']/@ref"></xsl:attribute>
       
       
            <rdfs:label>
                <xsl:value-of select="normalize-space(persName[@type='standard'])"/>
            </rdfs:label>  
            
            <xsl:choose>
                <xsl:when test="persName[@type='preferred']/text()">
                    <xsl:apply-templates select="persName[@type='preferred']"/>
                </xsl:when>                                     
            </xsl:choose>
            
            <xsl:choose>
                <xsl:when test="persName[@type='variant']/text()">
                    <xsl:apply-templates select="persName[@type='variant']"/>
                </xsl:when>                                     
            </xsl:choose> 
            
            <xsl:choose>
                <xsl:when test="death/date[@when]/text()">
                    <xsl:apply-templates select="death"/>
                </xsl:when>                                     
            </xsl:choose>
            
            <xsl:choose>
                <xsl:when test="birth/date[@when]/text()">
                    <xsl:apply-templates select="birth"/>
                </xsl:when>                                     
            </xsl:choose>                       
           
            <xsl:apply-templates select="occupation"/>
            
            <!--<xsl:apply-templates select="nationality"/>-->
            <!--<xsl:apply-templates select="note[@type='general']"/>-->
            <!--About <floruit>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            <!--About <trait>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            <!--About <respons>: CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->
            <xsl:apply-templates select="sex"/>
            
        </xsl:element>
    </xsl:template>

    
    <xsl:template match="persName[@type='preferred']">
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Name_of_Person">
                    <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
                    </rdfs:label>
                    <crm:P190_has_symbolic_content xml:lang="en">
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                    <crm:P2_has_type>
                        <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/personalName"/>
                    </crm:P2_has_type>
                    
                   <xsl:apply-templates select="child::name[@type='forename']"/> 
                    <xsl:apply-templates select="child::name[@type='middlename']"/>
                    <xsl:apply-templates select="child::name[@type='surname']"/>
                    <xsl:apply-templates select="child::genName"/>
                    <!--<xsl:apply-templates select="child::roleName"/>
                    CC & AD: There isn’t a application profile for this so we should leave it for now to be safe-->                                      
                    
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
    </xsl:template>
    
    <xsl:template match="persName[@type='variant']">
        <crm:P1_is_identified_by>
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Name_of_Person">
                <rdfs:label>Name of <xsl:value-of select="normalize-space(.)"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/personalName"/>
                </crm:P2_has_type>
                
                <xsl:apply-templates select="child::name[@type='forename']"/> 
                <xsl:apply-templates select="child::name[@type='middlename']"/>
                <xsl:apply-templates select="child::name[@type='surname']"/>
                <xsl:apply-templates select="child::genName"/>
                                                                
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P1_is_identified_by>

        <crm:P67i_is_referred_to_by>
           <xsl:apply-templates select="child::note"/>
        </crm:P67i_is_referred_to_by>
    </xsl:template>
 
 
                <!--NOTE ON NAMES : //Unique URIs situation:
               <uri:Name_of_Person> is being used to represent both a person and the name of the person when there should be a 
               URI for the person and a separate URI for their name as a whole. The URI for their name as a whole should then 
               be connected to all the different parts of their name (eg. <uri:First_Name_of_Person>, <uri:Middlename_of_Person>, etc.) 
               See Identifiers as parts of other identifiers for more info.
               Sidenote: Names and parts of names should be shared between people, but the URI for the person should be unique. Each 
               person is a unique entity, their names are not. 
                -->
 
    <xsl:template match="name[@type='forename']">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/First_Name_of_Person">
            <rdfs:label>Forename of <xsl:value-of select="normalize-space(ancestor::persName[@type='preferred'])"/>
            </rdfs:label>
            <crm:P190_has_symbolic_content xml:lang="en">
                <xsl:value-of select="normalize-space(.)"/>
            </crm:P190_has_symbolic_content>
            <crm:P2_has_type>
                <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/forename"/>
            </crm:P2_has_type>
        </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>
    </xsl:template>
        
       
    <xsl:template match="name[@type='middlename']">        
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Middlename_of_Person">
                <rdfs:label>Middlename of <xsl:value-of select="normalize-space(ancestor::persName[@type='preferred'])"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://temp.lincsproject.ca/Middlename"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="name[@type='surname']">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Surname_of_Person">
                <rdfs:label>Surname of <xsl:value-of select="normalize-space(ancestor::persName[@type='preferred'])"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/surname"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="genName">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/genName_of_Person">
                <rdfs:label>Generational name of <xsl:value-of select="normalize-space(ancestor::persName[@type='preferred'])"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/generationalName"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="roleName">
        <crm:P106_is_composed_of xml:lang="en">
            <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/roleName_of_Person">
                <rdfs:label>Role name of <xsl:value-of select="normalize-space(ancestor::persName[@type='preferred'])"/>
                </rdfs:label>
                <crm:P190_has_symbolic_content xml:lang="en">
                    <xsl:value-of select="normalize-space(.)"/>
                </crm:P190_has_symbolic_content>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/roleName"/>
                </crm:P2_has_type>
            </crm:E33_E41_Linguistic_Appellation>
        </crm:P106_is_composed_of>    
    </xsl:template>
    
    <xsl:template match="note">
        <crm:E33_Linguistic_Object rdf:about="http://temp.lincsproject.ca/Name_of_Person"> 
            <rdfs:label>Name of label : Note about the person's variant name</rdfs:label>
            <crm:P190_has_symbolic_content xml:lang="en">
                <xsl:value-of select="normalize-space(.)"/>
            </crm:P190_has_symbolic_content>
            <crm:P2_has_type>
                <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/note"/>
            </crm:P2_has_type>            
        </crm:E33_Linguistic_Object>        
    </xsl:template>
        
    <xsl:template match="birth">
        <crm:P98i_was_born>
            <crm:E67_Birth rdf:about="http://temp.lincsproject.ca/Name_of_Person">
                <rdfs:label>Birth event of <xsl:choose>
                    <xsl:when test="preceding-sibling::persName[@type='preferred']/text()">
                        <xsl:value-of select="normalize-space(preceding-sibling::persName[@type='preferred'])"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(preceding-sibling::persName[@type='standard'])"/>
                    </xsl:otherwise>                       
                </xsl:choose>
                </rdfs:label>               
                
               <xsl:apply-templates select="./date"/>
               <!--<xsl:apply-templates select="./placeName"/>
               CC: we need to be able to associate the place with the fact that the person was born there (just as we do with a timespan “this person had a birth time and this is the birthtime and its properties”). Leave it in comments, not converting, waiting for an official “How to connect placenames to birth and death events” from LINCS
               CC: They’ll have URIs for all the places coming from LEAF Writer-->
               
            </crm:E67_Birth>
        </crm:P98i_was_born>
    </xsl:template>
    
    <xsl:template match="death">
        <crm:P100i_died_in>
            <crm:E69_Death rdf:about="http://temp.lincsproject.ca/Name_of_Person">
                <rdfs:label>Death event of <xsl:choose>
                        <xsl:when test="preceding-sibling::persName[@type='preferred']/text()">
                            <xsl:value-of select="normalize-space(preceding-sibling::persName[@type='preferred'])"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="normalize-space(preceding-sibling::persName[@type='standard'])"/>
                        </xsl:otherwise>                       
                    </xsl:choose>
                </rdfs:label>
                <xsl:apply-templates select="./date"/>
                <!--<xsl:apply-templates select="./placeName"/>
                 CC: we need to be able to associate the place with the fact that the person was born there (just as we do with a timespan “this person had a birth time and this is the birthtime and its properties”). Leave it in comments, not converting, waiting for an official “How to connect placenames to birth and death events” from LINCS
                CC: They’ll have URIs for all the places coming from LEAF Writer-->
                <!--Information pointing to the location of Birth will go in here when we have the CIDOC pattern for Birth locations-->
                
            </crm:E69_Death>
        </crm:P100i_died_in>
    </xsl:template>
    
    <xsl:template match="birth/date">
        <crm:P4_has_time-span>
            <crm:E52_Time-Span rdf:about="http://temp.lincsproject.ca/Time_Span"> <!-- URI specific to the event or specific to the timespan -->
                    <rdfs:label>Date of birth event of <xsl:choose>
                        <xsl:when test="../preceding-sibling::persName[@type='preferred']/text()">
                            <xsl:value-of select="normalize-space(../preceding-sibling::persName[@type='preferred'])"/>
                        </xsl:when>
                        <xsl:otherwise>
                            <xsl:value-of select="normalize-space(../preceding-sibling::persName[@type='standard'])"/>
                        </xsl:otherwise>                       
                    </xsl:choose>
                    </rdfs:label>
                <crm:P82_at_some_time_within><xsl:value-of select="@when"/></crm:P82_at_some_time_within>
                <crm:P82b_end_of_the_end><xsl:value-of select="@when"/>^^xsd:dateTime</crm:P82b_end_of_the_end>
                <crm:P82a_begin_of_the_begin><xsl:value-of select="@when"/>^^xsd:dateTime</crm:P82a_begin_of_the_begin> 
            </crm:E52_Time-Span>
        </crm:P4_has_time-span>
    </xsl:template>
    
    <!--AD: Not transforming yet, because we don't know how to connect the E53 Place to the birth and death event, missing a P_property?
        <xsl:template match="birth/placeName">        
        <crm:E53_Place rdf:about="http://temp.lincsproject.ca/Name_of_Place?">
            <rdfs:label>
                <xsl:value-of select="normalize-space(.)"/>
            </rdfs:label>            
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#birthPlace">
                    <crm:P190_has_symbolic_content>
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
        </crm:E53_Place>        
    </xsl:template>-->
    
    <xsl:template match="death/date">
        <crm:P4_has_time-span>
            <crm:E52_Time-Span rdf:about="http://temp.lincsproject.ca/Time_Span"><!-- URI specific to the event or specific to the timespan -->
               <rdfs:label>Date of death event of <xsl:choose>
                    <xsl:when test="../preceding-sibling::persName[@type='preferred']/text()">
                        <xsl:value-of select="normalize-space(../preceding-sibling::persName[@type='preferred'])"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(../preceding-sibling::persName[@type='standard'])"/>
                    </xsl:otherwise>                       
                </xsl:choose>
                </rdfs:label>
                <crm:P82_at_some_time_within><xsl:value-of select="@when"/></crm:P82_at_some_time_within>
                <crm:P82b_end_of_the_end><xsl:value-of select="@when"/>^^xsd:dateTime</crm:P82b_end_of_the_end>
                <crm:P82a_begin_of_the_begin><xsl:value-of select="@when"/>^^xsd:dateTime</crm:P82a_begin_of_the_begin> 
            </crm:E52_Time-Span>
        </crm:P4_has_time-span>
    </xsl:template>
    
    <!-- AD: Not transforming yet, because we don't know how to connect the E53 Place to the birth and death event, missing a P_property?
        <xsl:template match="death/placeName">        
        <crm:E53_Place rdf:about="http://temp.lincsproject.ca/Name_of_Place?">
            <rdfs:label><xsl:value-of select="normalize-space(.)"/></rdfs:label>            
            <crm:P1_is_identified_by>
                <crm:E33_E41_Linguistic_Appellation rdf:about="https://sparql.cwrc.ca/ontologies/cwrc.html#deathPlace">
                    <crm:P190_has_symbolic_content>
                        <xsl:value-of select="normalize-space(.)"/>
                    </crm:P190_has_symbolic_content>
                </crm:E33_E41_Linguistic_Appellation>
            </crm:P1_is_identified_by>
        </crm:E53_Place>        
    </xsl:template>-->
      
    <!-- Mapping the NATIONALITY
            1 - Define a variable to store the beginning of our URI. -->
    <xsl:variable name="nationalityPrefix" 
        select="' http://id.lincsproject.ca/cwrc/'"/> <!-- Don't map this, there too many values-->
    <!-- 2 - Define a variable to store the map then create the map.-->
    <xsl:variable name="nationality" as="map(xs:string, xs:string)">
        <xsl:map>
            <xsl:map-entry key="'africanNationalIdentity'" select="'africanNationalIdentity'"/>
            <xsl:map-entry key="'anglo-IndianNationalIdentity'" select="'anglo-IndianNationalIdentity'"/>
            <xsl:map-entry key="'anglo-IrishNationalIdentity'" select="'anglo-IrishNationalIdentity'"/>
            <xsl:map-entry key="'britishNationalIdentity'" select="'britishNationalIdentity'"/>
            <xsl:map-entry key="'europeanNationalIdentity'" select="'europeanNationalIdentity'"/>
            <xsl:map-entry key="'florentineNationalIdentity'" select="'florentineNationalIdentity'"/>
            <xsl:map-entry key="'hanoverianNationalIdentity'" select="'hanoverianNationalIdentity'"/>
            <xsl:map-entry key="'jewishNationalIdentity'" select="'jewishNationalIdentity'"/>
            <xsl:map-entry key="'mohawkNationalIdentity'" select="'mohawkNationalIdentity'"/>
            <xsl:map-entry key="'saxonNationalIdentity'" select="'saxonNationalIdentity'"/>
            <xsl:map-entry key="'sephardicJewishNationalIdentity'" select="'sephardicJewishNationalIdentity'"/>
            <xsl:map-entry key="'statelessNationalIdentity'" select="'statelessNationalIdentity'"/>            
        </xsl:map>
    </xsl:variable>
    
   <xsl:template match="nationality">
        <crm:P74_has_current_or_former_residence>
            <crm:E53_Place rdf:about="http://temp.lincsproject.ca/Name_of_Place">
                <rdfs:label>Nationality of the person</rdfs:label>
                <crm:P1_is_identified_by>
                    <crm:E33_E41_Linguistic_Appellation rdf:about="http://temp.lincsproject.ca/Name_of_Place">
                        <rdfs:label>Name of the place where a person is from</rdfs:label>
                        <crm:P190_has_symbolic_content xml:lang="en">
                            <xsl:value-of select="normalize-space(.)"/> : nationality of the person e.g. the place where they live
                        </crm:P190_has_symbolic_content>
                        <crm:P2_has_type>
                            <xsl:choose>
                                <xsl:when test="map:contains($nationality, current())">
                                    <crm:E55_Type rdf:about="{concat($nationalityPrefix, $nationality(current()), '')}"/>
                                </xsl:when>
                                <xsl:otherwise>
                                    <xsl:comment>ERROR : No URI was found for nationality : "<xsl:value-of select="current()"/>". Please check the content of the @value or rely on our documentation to input correct format.</xsl:comment>
                                </xsl:otherwise>
                            </xsl:choose>         
                        </crm:P2_has_type>                        
                    </crm:E33_E41_Linguistic_Appellation>
                </crm:P1_is_identified_by>
            </crm:E53_Place>
        </crm:P74_has_current_or_former_residence>
    </xsl:template>
    
       <xsl:template match="occupation">
        <crm:P14i_performed>
            <frbroo:F51_Pursuit rdf:about="http://temp.lincsproject.ca/Pursuit_of_Name_of_Person"> 
                <!-- URI = Name of Person ? or of concept for "Pursuit"?-->
                <rdfs:label>Occupation of the person</rdfs:label>
                <crm:P2_has_type>
                    <xsl:element name="{concat('crm:', 'E55_Type')}">
                        <xsl:attribute name="rdf:about" select="@source"></xsl:attribute>
                    </xsl:element>  
                </crm:P2_has_type>
            </frbroo:F51_Pursuit>
        </crm:P14i_performed>
    </xsl:template>
    
    <xsl:template match="sex">
        <crm:P140i_was_attributed_by>
            <crm:E13_Attribute_Assignment>
                <rdfs:label>Gender of <xsl:choose>
                    <xsl:when test="../preceding-sibling::persName[@type='preferred']/text()">
                        <xsl:value-of select="normalize-space(./preceding-sibling::persName[@type='preferred'])"/>
                    </xsl:when>
                    <xsl:otherwise>
                        <xsl:value-of select="normalize-space(./preceding-sibling::persName[@type='standard'])"/>
                    </xsl:otherwise>                       
                </xsl:choose>
                </rdfs:label>
                <crm:P2_has_type>
                    <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/GenderContext"/>            
                </crm:P2_has_type>
                <crm:P141_assigned>
                    <crm:E7_Activity>
                        <rdfs:label>Activity of the gender event</rdfs:label>
                        <crm:P2_has_type>
                            <crm:E55_Type rdf:about="http://id.lincsproject.ca/cwrc/GenderEvent"/>            
                        </crm:P2_has_type>
                        <xsl:element name="{concat('crm:', 'P16_used_specific_object')}">
                            <xsl:attribute name="rdf:resource" select="@source"></xsl:attribute>
                        </xsl:element>  
                    </crm:E7_Activity>                    
                </crm:P141_assigned>
            </crm:E13_Attribute_Assignment>
        </crm:P140i_was_attributed_by>   
    </xsl:template>
</xsl:stylesheet>
