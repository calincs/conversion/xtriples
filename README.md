# XTriples

This repo was originally mirrored from here: `https://github.com/digicademy/xtriples`

[![DOI](https://zenodo.org/badge/24633761.svg)](https://zenodo.org/badge/latestdoi/24633761)

A generic webservice to extract RDF statements from XML resources. With this webservice you can crawl XML repositories and extract RDF statements
using a simple configuration based on XPATH expressions.

![Structure of the XTriples webservice](resources/images/structure.png "XTriples structure")

## Run XTriples locally

```bash
git clone git@gitlab.com:calincs/conversion/xtriples.git
cd xtriples
docker compose up
```

Open the ExistDB UI with the Xtriples app at [http://localhost:8080](http://localhost:8080)

Any23 available at [http://localhost:8081](http://localhost:8081)

## Wiki

Check out the wiki [here](https://gitlab.com/calincs/conversion/xtriples/-/wikis/home).
